var Graph = require("./include/dijkstra.js");

module.exports = {
  pathFind: function(start, end, g){
    // Encode points to correspond with graph keys
    var startNode = '[' + start.x + ', ' + start.y + ']';
    var endNode = '[' + end.x + ', ' + end.y + ']';
    
    // Create the graph from the graph map and find shortest path
    var graph = new Graph(g);
    var encodedPath = graph.findShortestPath(startNode, endNode);
    var path = [];
    
    if(encodedPath){
      encodedPath.forEach(function(point){
        // Decode path
        var res = point.match(/\d+/g);
        path.push({
          x: res[0],
          y: res[1]
        });
      });
      return path;
    } else {
      return null;
    }
  }
}