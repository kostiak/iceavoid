angular.module('iceavoid')
  .directive('map', ['mapData', function(mapData){
    return {
      restrict: 'E',
      scope: {
        mode: '='
      },
      template: '<canvas width="500" height="500"></canvas>',
      link: function(scope, element) {
        
        var firstPoint = true;
        var canvas = element.find("canvas")[0];
        var newPolygon = [];
        
        //redraw when data changes
        scope.$watchCollection(function(){
          return mapData;
        }, function(){
          redraw(canvas, mapData, newPolygon);
        });
        
        //if mode changes to finish-polygon create the polygon from newPolygon points, 
        // reset it, and redraw the canvas
        scope.$watch('mode', function(){
          if(scope.mode == 'finish-polygon'){
            finishPolygon(newPolygon, mapData);
            
            newPolygon = [];
            scope.mode = '';
            
            redraw(canvas, mapData, newPolygon);
          }
        })
        
        //Listening to mouse clicks
        canvas.addEventListener('click', function(event) {
          var point = {
            x: event.offsetX,
            y: event.offsetY
          }
          // In route mode
          if(scope.mode === 'route'){
            if(firstPoint) {
              mapData.start = point;
              firstPoint = false;
            } else {
              mapData.end = point;
              firstPoint = true;
              scope.mode = '';
            }
            scope.$apply();
          // In polygon mode
          } else if(scope.mode === 'polygon'){
            newPolygon.push(point);
            redraw(canvas, mapData, newPolygon);
          }
        });
 
      }
    };
  }]);
  

// Helper functions
  
function drawRect(ctx, x, y, color){
  ctx.beginPath();
  ctx.fillStyle = color || 'red';
  ctx.rect(x - 3, y - 3, 6, 6);
  ctx.fill();
}

function drawPath(ctx, points){
  if(points.length > 0){
    ctx.strokeStyle = "#EC971F";
    ctx.beginPath();
    ctx.moveTo(points[0].x, points[0].y);
    points.forEach(function(point){
      ctx.lineTo(point.x, point.y);
    });
    
    ctx.stroke();
  }
}

function drawPolygon(ctx, points){
  if(points.length > 0){
    ctx.fillStyle = "#ADD8E6";
    ctx.beginPath();
    ctx.moveTo(points[0].x, points[0].y);
    points.forEach(function(point){
      ctx.lineTo(point.x, point.y);
    });
    ctx.closePath();
    ctx.fill();
  }
}

function finishPolygon(newPolygon, mapData){
  if(newPolygon.length > 2){
    //find center
    var cent = findCenter(newPolygon);

    // find angles
    findAngles(cent, newPolygon);
    
    // sort by angles
    newPolygon.sort(function(a, b) {
      return (a.angle >= b.angle) ? 1 : -1
    });
    
    mapData.polygons.push(newPolygon);
  }
}

function redraw(canvas, mapData, newPolygon){
  
  var ctx = canvas.getContext('2d');
  
  ctx.fillStyle = "white";
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fill();
  
  ctx.fillStyle = "white";
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fill();
  
  mapData.polygons.forEach(function(points){
    drawPolygon(ctx, points)
  });
  
  newPolygon.forEach(function(point){
    drawRect(ctx, point.x, point.y, 'green');
  })
  
  drawRect(ctx, mapData.start.x, mapData.start.y);
  drawRect(ctx, mapData.end.x, mapData.end.y);
  
  drawPath(ctx, mapData.path);
}

function findCenter(points){
  var x = 0;
  var y = 0;
  points.forEach(function(point){
    x += point.x;
    y += point.y;
  });
  
  return {
    x: x / points.length,
    y: y / points.length
  }
}


function findAngles(c, points) {
  var dx, dy;
  points.forEach(function(p){
    dx = p.x - c.x;
    dy = p.y - c.y;
    p.angle = Math.atan2(dy, dx);
  });
}