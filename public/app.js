'use strict';

// Declare app level module which depends on views
var app = angular.module('iceavoid', [
  'ui.router'
])

app.config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/");

    $stateProvider.state({
       name: 'main',
       url: '/',
       templateUrl: 'views/main/index.html',
       controller: 'MainCtrl',
       controllerAs: 'main'
    });
});