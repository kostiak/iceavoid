'use strict';

angular.module('iceavoid').controller('MainCtrl', ['mapData', 'api', 'demo', function(mapData, api, demo) {
  var self = this;
  
  // Begins point A/B definition mode
  this.defineRoute = function(){
    self.mode = 'route';
    mapData.resetPoints();
  }
  
  // Begins polygon creation mode
  this.createPolygon = function(){
    self.mode = 'polygon';
  }
  
  // Finishes polygon creation mode
  this.finishPolygon = function(){
    self.mode = 'finish-polygon';
    mapData.path = [];
  }
  
  // Displays the demo
  this.demo = function(){
    demo.create();
  }
  
  // Resets the data
  this.reset = function(){
    mapData.reset();
  }
  
  // Finds the shortest path
  this.solve = function(){
    api.solve();
  }
  
  this.mode = '';
  this.mapData = mapData;
  
}]);