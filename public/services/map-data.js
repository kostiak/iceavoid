angular.module('iceavoid')
  .service('mapData', [function(){
    this.start = {};
    this.end = {};
    this.polygons = [];
    this.path = [];
    this.resetPoints = () => {
      this.start = {};
      this.end = {};
      this.path = [];
    }
    this.reset = () => {
      this.resetPoints();
      this.polygons = [];
    }
  }]);