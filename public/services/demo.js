angular.module('iceavoid')
  .service('demo', ['api', 'mapData', function(api, mapData){
    this.create = function(){
      mapData.start = {
        x: 250,
        y: 100
      }
      mapData.end = {
        x: 250,
        y: 400
      }
      var polygon1 = [{
        x: 100,
        y: 100
      }, {
        x: 200,
        y: 200
      }, {
        x: 400,
        y: 150
      }];
    
      
      var polygon2 = [{
        x: 150,
        y: 250
      }, {
        x: 360,
        y: 250
      },{
        x: 360,
        y: 350
      },{
        x: 150,
        y: 350
      }]
      mapData.polygons = [];
      mapData.polygons.push(polygon1);
      mapData.polygons.push(polygon2);
      
      api.solve();
      
    }
    // this.create = function(){
    //   mapData.start = {
    //     x: getRandomArbitrary(0,500),
    //     y: getRandomArbitrary(0,100)
    //   };
    //   mapData.end = {
    //     x: getRandomArbitrary(0, 500),
    //     y: getRandomArbitrary(400,500)
    //   }
    //   for(var i=0; i<=getRandomArbitrary(1, 3); i++){
    //     var polygon = [];
    //     for(var j=0; j<=getRandomArbitrary(3,6); j++){
    //       polygon.push({
    //         x: getRandomArbitrary(0, 100) + 100*i,
    //         y: getRandomArbitrary(110, 390)
    //       })
    //     }
    //     mapData.polygons.push(polygon);
    //   }
    //   api.solve();
    // }  
    // function getRandomArbitrary(min, max) {
    //   return Math.random() * (max - min) + min;
    // }
  }]);
  
  