angular.module('iceavoid')
  .service('api', ['mapData', '$http', function(mapData, $http){
    this.solve = function(){
      $http.post('https://projects-kostiak.c9users.io:8080/api', {
        start: mapData.start,
        end: mapData.end,
        polygons: mapData.polygons
      }).then(function(res){
        mapData.path = res.data;
      })
    }
  }]);