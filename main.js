const express = require('express');
const bodyParser = require('body-parser')
const app = express()  
const port = process.env.PORT || 3000

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var pathFiner = require("./pathFinder.js");
var graphCreator = require("./graphCreator.js");

// Accept json with {x: , y:} start and end objects and 
//  an array of polygons, each of which is an array of points
app.post('/api', function(req, res) {
  //create a graph of all possible connections where they connection does not intersect any polygons
  var graph = graphCreator.create(req.body.start, req.body.end, req.body.polygons);
  
  //calculate the shortest path
  var path = pathFiner.pathFind(req.body.start, req.body.end, graph);

  res.send(JSON.stringify(path));
});

app.listen(port, (err) => {  
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
});