# IceAvoid #

## An app that lets you place icebergs and calculate the shortest path from A to B ##


### To get started: ###

* in `\` run `npm install`

* in `\public` run `bower install`

* run `npm main.js` to start the server and go to `localhost:3000`