var SAT = require("./include/sat-js/SAT.js");

// The distance between two points
function distance(p1, p2){
  var a = p1.x - p2.x
  var b = p1.y - p2.y

  return Math.sqrt( a*a + b*b );
}

// Compares a line to a list of polygons and checks if they intersect
function lineIntersectsPolygons(line, polygons){
  for(var i=0; i<polygons.length; i++){
    var res = new SAT.Response();
    if(SAT.testPolygonPolygon(line, polygons[i], res)){
      if(res.overlap > 2){
          return true; 
      }
    }
  }
  return false;
}

// Get start, end points and array of polygons
// Return all an a flattended array with all the points.
function collectPoints(start, end, polyArr){
  var points = [];
  
  points.push(start);
  points.push(end);
  polyArr.forEach(function(polygon){
    polygon.forEach(function(point){
      points.push(point);
    })
  })
  
  return points;
}

// encode point for graph map
function pToString(point){
  return "[" + point.x + ", " + point.y + "]"
}

module.exports = {
  create: function(start, end, polyArr){
    var graphMap = {};
    
    // Get an array of all possible nodes
    var points = collectPoints(start, end, polyArr);
    var polygons = [];
    
    // convert polygon point list to SAT.Polygon for intersection checks
    polyArr.forEach(function(poly){
      if(poly.length > 0){
        var v1 = new SAT.Vector(poly[0].x, poly[0].y);
        var vectors = [];
        poly.forEach(function(point){
            vectors.push(new SAT.Vector(point.x - v1.x, point.y - v1.y));
        });
        polygons.push(new SAT.Polygon(v1, vectors));
      }
    });
    
    // Take every 2 unique point pairs and 
    for(var i=0; i<points.length; i++){
      for(var j=i+1; j<points.length; j++){
        var p1 = points[i];
        var p2 = points[j];
        
        // Create a line between them representing a vertex on the graph
        var line = new SAT.Polygon(p1, [
          new SAT.Vector(),
          new SAT.Vector(p2.x - p1.x, p2.y - p1.y)
        ]);

        // Check if the line intersects any polygons
        if(!lineIntersectsPolygons(line, polygons)){
          // if not, add them to the graph map
          if(!graphMap[pToString(p1)]) graphMap[pToString(p1)] = {};
          graphMap[pToString(p1)][pToString(p2)] = distance(p1, p2);
          
          if(!graphMap[pToString(p2)]) graphMap[pToString(p2)] = {};
          graphMap[pToString(p2)][pToString(p1)] = distance(p1, p2);
        }
      }
    }

    return graphMap;
  }
}